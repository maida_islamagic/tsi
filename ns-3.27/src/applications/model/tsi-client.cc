/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007,2008,2009 INRIA, UDCAST
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amine Ismail <amine.ismail@sophia.inria.fr>
 *                      <amine.ismail@udcast.com>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "tsi-client.h"
#include "ns3/tsi-header.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"


#include "ns3/address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/node.h"
#include "ns3/nstime.h"
#include "ns3/random-variable-stream.h"
#include "ns3/trace-source-accessor.h"
#include "onoff-application.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/string.h"
#include "ns3/pointer.h"


#include <cstdlib>
#include <cstdio>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("TsiClient");

NS_OBJECT_ENSURE_REGISTERED (TsiClient);

TypeId
TsiClient::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::TsiClient")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<TsiClient> ()
    .AddAttribute ("MaxPackets",
                   "The maximum number of packets the application will send",
                   UintegerValue (100),
                   MakeUintegerAccessor (&TsiClient::m_count),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("FlowNumber",
                   "The number of flow",
                   UintegerValue (1),
                   MakeUintegerAccessor (&TsiClient::m_flow),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("Interval",
                   "The time to wait between packets", TimeValue (Seconds (1.0)),
                   MakeTimeAccessor (&TsiClient::m_interval),
                   MakeTimeChecker ())
   /* .AddAttribute ("Interval",
                   "The time to wait between packets", TimeValue (Seconds (0.020)),
                   MakeTimeAccessor (&TsiClient::m_interval_g722),
                   MakeTimeChecker ())
    .AddAttribute ("Interval",
                   "The time to wait between packets", TimeValue (Seconds (0.015)),
                   MakeTimeAccessor (&TsiClient::m_interval_g729),
                   MakeTimeChecker ())*/
    .AddAttribute ("RemoteAddress",
                   "The destination Address of the outbound packets",
                   AddressValue (),
                   MakeAddressAccessor (&TsiClient::m_peerAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort", "The destination port of the outbound packets",
                   UintegerValue (100),
                   MakeUintegerAccessor (&TsiClient::m_peerPort),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("PacketSize",
                   "Size of packets generated. The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.",
                   UintegerValue (1024),
                   MakeUintegerAccessor (&TsiClient::m_size),
                   MakeUintegerChecker<uint32_t> (12,1500)) 
     .AddAttribute ("Type",
		   "Type of the Codec (G.711=0, G.722 = 1, G.729 = 2).",
		   UintegerValue (0),
		   MakeUintegerAccessor (&TsiClient::m_type),
		   MakeUintegerChecker<uint32_t> (0,3))

    .AddTraceSource ("Tx",
                     "A packet has been sent",
                     MakeTraceSourceAccessor (&TsiClient::m_txTrace),
                     "ns3::Packet::AddressTracedCallback")
  ;
  return tid;
}

TsiClient::TsiClient ()
{
  NS_LOG_FUNCTION (this);
  m_sent = 0;
  m_socket = 0;
  m_flow = 0;
  m_type = 0;
 // m_interval= 1.0; //
  m_sendEvent = EventId ();
}

TsiClient::~TsiClient ()
{
  NS_LOG_FUNCTION (this);
}

void
TsiClient::SetRemote (Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = ip;
  m_peerPort = port;
}

void
TsiClient::SetRemote (Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_peerAddress = addr;
}
 

void
TsiClient::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}
//dodali funckiju
void
IntTrace (Time oldValue, Time newValue)
{
  //std::cout << "Traced " << oldValue << " to " << newValue << std::endl;
}


void
TsiClient::StartApplication (void)
{
  NS_LOG_FUNCTION (this);
//dodali 2 linije
//Ptr<TsiClient> tsiclient = CreateObject<TsiClient> ();
 //tsiclient->TraceConnectWithoutContext ("Tx", MakeCallback (&IntTrace));

  if (m_type == 0) //G.711
        {
             m_interval = Seconds(0.000125);
            // m_cbrRate = DataRate ("64kbps");
             // std::string m_cbrRate = "100Mbps";
             m_size = 1000; //50*20
               
        }
  
   if (m_type == 1) //G.722
        {
             m_interval = Seconds(0.020);
            // m_cbrRate = DataRate("64kbps");
             m_size = 1000; //50*20
               
        }
   if (m_type == 2) //G.729
        {
             m_interval = Seconds(0.015);
            // m_cbrRate = DataRate("8kbps");
             m_size = 1000; //50*20
               
        }

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (InetSocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else
        {
          NS_ASSERT_MSG (false, "Incompatible address type: " << m_peerAddress);
        }
    }

  m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
  m_socket->SetAllowBroadcast (true);
  m_sendEvent = Simulator::Schedule (Seconds (0.0), &TsiClient::Send, this);
}

void
TsiClient::StopApplication (void)
{
  NS_LOG_FUNCTION (this);
  Simulator::Cancel (m_sendEvent);
}

void
TsiClient::Send (void)
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT (m_sendEvent.IsExpired ());

  TsiHeader tsiHead;
  tsiHead.SetSeq (m_sent);
  tsiHead.SetFlow (m_flow);
  
  Ptr<Packet> p = Create<Packet> (m_size-(8+4)); // 8+4 : the size of the tsiHead header

  //std::ostringstream msg; msg << "Tsi primjer!" << '\0';
  //Ptr<Packet> p = Create<Packet> ((uint8_t*) msg.str().c_str(), msg.str().length());
  
  p->AddHeader (tsiHead);

  std::stringstream peerAddressStringStream;
  if (Ipv4Address::IsMatchingType (m_peerAddress))
    {
      peerAddressStringStream << Ipv4Address::ConvertFrom (m_peerAddress);
    }
 
  if ((m_socket->Send (p)) >= 0)
    {
      ++m_sent;
      NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
                                    << peerAddressStringStream.str () << " Uid: "
                                    << p->GetUid () << " Time: "
                                    << (Simulator::Now ()).GetSeconds ());

    }
  else
    {
      NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
                                          << peerAddressStringStream.str ());
    }

  if (m_sent < m_count)
    {
      m_sendEvent = Simulator::Schedule (m_interval, &TsiClient::Send, this);
    }
    m_txTrace (p);
}

} // Namespace ns3
