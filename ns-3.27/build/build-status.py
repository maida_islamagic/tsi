#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/subdir/ns3.27-subdir-debug', 'build/scratch/ns3.27-lena-x2-handover-debug', 'build/scratch/ns3.27-scratch-simulator-debug', 'build/scratch/ns3.27-tsi_test-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

